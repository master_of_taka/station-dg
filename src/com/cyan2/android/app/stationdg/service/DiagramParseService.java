package com.cyan2.android.app.stationdg.service;

import com.cyan2.android.app.stationdg.activity.MyStations;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.AttributeSet;
import android.widget.ProgressBar;

public class DiagramParseService extends Service{
	
	public static final String NOTIFICATION_ID_KEY = "com.cyan2.android.app.stationdg.service.DiagramParseService.NOTIFICATION_ID_KEY";
	
	@Override
	public void onStart(Intent intent, int startId) {
		
		NotificationManager manager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
		
		Notification n = new Notification();
		n.icon = android.R.drawable.stat_sys_download;
		n.tickerText = "マイステーションを登録しています";
		n.when = System.currentTimeMillis();
		n.flags = Notification.FLAG_ONGOING_EVENT;
		
		Intent in = new Intent(getApplicationContext(), MyStations.class);
		in.putExtra(NOTIFICATION_ID_KEY, 1);
		PendingIntent pe = PendingIntent.getActivity(getApplicationContext(), 0, in, PendingIntent.FLAG_UPDATE_CURRENT);
		n.setLatestEventInfo(getApplicationContext(), "タイトル","内容", pe);
		
		manager.notify(1, n);
		
		try{
			Thread.sleep(10000L);
		}catch(Exception e){
			
		}
		manager.cancel(1);
		
		n.icon = android.R.drawable.stat_sys_download_done;
		n.tickerText = "マイステーションを登録しました";
		n.when = System.currentTimeMillis();
		n.flags = Notification.FLAG_AUTO_CANCEL;
		n.setLatestEventInfo(getApplicationContext(), "タイトル","内容", pe);
		manager.notify(1, n);
		
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
	
}	
