package com.cyan2.android.app.stationdg.activity;

import java.util.List;

import com.cyan2.android.app.stationdg.R;
import com.cyan2.android.app.stationdg.domain.Line;
import com.cyan2.android.app.stationdg.domain.Station;
import com.cyan2.android.app.stationdg.parser.LineNameSearchReader;
import com.cyan2.android.app.stationdg.service.DiagramParseService;
import com.cyan2.android.app.stationdg.util.NetworkUtils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class LineSearch extends Activity implements Runnable{
	
	private ProgressDialog progressDialog;
	
	private Station station;
	
	private List<Line> lines;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_line_search);
		
		Intent intent = getIntent();
		station = (Station)intent.getSerializableExtra(Station.class.getName());
		
		progressDialog = new ProgressDialog(this);
		progressDialog.setTitle("取得中");
		progressDialog.setMessage("データ取得中・・・");
		progressDialog.setIndeterminate(false);
		progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
			
			public void onCancel(DialogInterface dialog) {
				if(lines == null || lines.isEmpty()){
					Toast.makeText(LineSearch.this, "検索結果がありません", Toast.LENGTH_LONG).show();
					return;
				}

				ArrayAdapter<Line> adapter = new ArrayAdapter<Line>(LineSearch.this, android.R.layout.simple_list_item_1,lines);
				
				ListView listView = (ListView)findViewById(R.id.line_name_list_view);
				listView.setAdapter(adapter);
				listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

					public void onItemClick(AdapterView<?> parent, View view,
							int position, long id) {
						ListView listView = (ListView) parent;
						// クリックされたアイテムを取得します
						Line item = (Line) listView.getItemAtPosition(position);
						
						Intent intent = new Intent(LineSearch.this,DiagramParseService.class);
//						intent.setClassName(getPackageName(),
//								getClass().getPackage().getName() + ".DiagramRegist");
						intent.putExtra(Station.class.getName(), station);
						intent.putExtra(Line.class.getName(), item);
//						
						LineSearch.this.startService(intent);
						
						Log.d("station-dg", "started service on  linesearch");
						
						Toast.makeText(LineSearch.this, "登録開始しました", Toast.LENGTH_LONG).show();
						
//						LineSearch.this.startActivity(intent);
//						finish();
					}
				});
			}
		});
		
		progressDialog.show();
		
		Thread thread = new Thread(this);
		thread.start();
		
	}

	public void run() {
		getLines();
		progressDialog.cancel();
	}
	
	private void getLines(){
		if(!NetworkUtils.isConnected(getApplicationContext())){
			new AlertDialog.Builder(LineSearch.this)
			.setTitle("ネットワークがみつかりません")
			.setMessage("ネットワークに接続してから再度実行してください")
			.setNegativeButton("キャンセル", null)
			.show();
			return;
		}
		
		LineNameSearchReader r = new LineNameSearchReader();
		r.setStationCode(station.getStationCode());
		
		try {
			lines = r.get();
		} catch (Exception e) {
			Log.w("diagram-dg", "error",e);
		}
	}
}
