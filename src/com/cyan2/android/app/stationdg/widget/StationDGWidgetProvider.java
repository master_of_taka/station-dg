package com.cyan2.android.app.stationdg.widget;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.widget.RemoteViews;

import com.cyan2.android.app.stationdg.R;
import com.cyan2.android.app.stationdg.db.DBManager;
import com.cyan2.android.app.stationdg.domain.Diagram;
import com.cyan2.android.app.stationdg.domain.DiagramDetail;
import com.cyan2.android.app.stationdg.logic.DiagramDetailLogic;

public class StationDGWidgetProvider extends AppWidgetProvider {
	private static final String ACTION_START_MY_ALARM = "com.cyan2.android.app.stationdg.widget.StationDGWidgetProvider.ACTION_START_MY_ALARM";

	private final long interval = 60 * 1000;
	
	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {

		for(int i : appWidgetIds){
			Log.d("station-dg", "onUpdate:" + i);
		}

		setAlarm(context);
	}
	
	
	
	@Override
	public void onDisabled(Context context) {
		Intent alarmIntent = new Intent(context, StationDGWidgetProvider.class);
		alarmIntent.setAction(ACTION_START_MY_ALARM);
		PendingIntent operation = PendingIntent.getBroadcast(context, 0, alarmIntent, 0);
		AlarmManager am = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
		am.cancel(operation);
	}

	@Override
	public void onDeleted(Context context, int[] appWidgetIds) {

		for (int i : appWidgetIds) {
			StationDGAppWidgetConfigure.deleteTitlePref(context, i);
		}
	}

	@Override
	public void onReceive(Context context, Intent intent) {

		super.onReceive(context, intent);
		if (intent.getAction().equals(ACTION_START_MY_ALARM)) {
			if (ACTION_START_MY_ALARM.equals(intent.getAction())) {
				Intent serviceIntent = new Intent(context, MyService.class);
				context.startService(serviceIntent);
			}
//			setAlarm(context);
		}
	}

	private void setAlarm(Context context) {
		
		Intent alarmIntent = new Intent(context, StationDGWidgetProvider.class);
		alarmIntent.setAction(ACTION_START_MY_ALARM);
		PendingIntent operation = PendingIntent.getBroadcast(context, 0, alarmIntent, 0);
		AlarmManager am = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
//		long now = System.currentTimeMillis() + 1; // + 1 は確実に未来時刻になるようにする保険
//		long oneHourAfter = now + interval - now % (interval);
		am.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, 5, interval, operation);
	}


	static void updateAppWidget(Context context,AppWidgetManager appWidgetManager,int appWidgetId,Diagram diagram){
		
		DBManager dbManager = new DBManager(context);
		Log.d("station-dg","db hash back:"+dbManager.getReadableDatabase().hashCode());
		try {
			RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.widget_one);
			views.setTextViewText(R.id.app_widget_station_name, diagram.getStationName());
			views.setTextViewText(R.id.app_widget_line_name, diagram.getLineName());
			views.setTextViewText(R.id.app_widget_district_name, diagram.getDistrictName());
			
			DiagramDetailLogic logic = new DiagramDetailLogic();
			List<DiagramDetail> diagramDetails = logic.find(dbManager, diagram, new Date(), 2) ;
			if(diagramDetails.isEmpty()){
				//時刻表がすでに削除されていたときの処理
				for(int i = 0;i < 2;i++){
					DiagramDetail dd = new DiagramDetail();
					dd.setDiagramId(-1);
					dd.setDepartureTime("");
					dd.setKindId("");
					dd.setKindName("");
					dd.setDestination("");
					
					diagramDetails.add(dd);
				}
			}
			
			//時間
			views.setTextViewText(R.id.app_widget_departure_time_1, diagramDetails.get(0).getDepartureTime());

			//種別
			views.setTextViewText(R.id.app_widget_kind_name_1, diagramDetails.get(0).getKindName());
			views.setFloat(R.id.app_widget_kind_name_1, "setTextScaleX", getTextScaleX(diagramDetails.get(0).getKindName(),2));
			views.setTextColor(R.id.app_widget_kind_name_1,getColor(context, diagramDetails.get(0).getKindId()));

			//行き先
			views.setTextViewText(R.id.app_widget_destination_1, diagramDetails.get(0).getDestination());
			views.setFloat(R.id.app_widget_destination_1, "setTextScaleX", getTextScaleX(diagramDetails.get(0).getDestination(),5));
			
			//特殊
			if(logic.isFinal(dbManager, diagramDetails.get(0))){
				views.setTextViewText(R.id.app_widget_extra_1, "最終");
			}else if(logic.isFirst(dbManager, diagramDetails.get(0))){
				views.setTextViewText(R.id.app_widget_extra_1, "始発");
			}else{
				views.setTextViewText(R.id.app_widget_extra_1, "  ");
			}
			
			views.setTextViewText(R.id.app_widget_departure_time_2, diagramDetails.get(1).getDepartureTime());
			views.setTextViewText(R.id.app_widget_kind_name_2, diagramDetails.get(1).getKindName());
			views.setFloat(R.id.app_widget_kind_name_2, "setTextScaleX", getTextScaleX(diagramDetails.get(1).getKindName(),2));
			views.setTextColor(R.id.app_widget_kind_name_2,getColor(context, diagramDetails.get(1).getKindId()));
			views.setTextViewText(R.id.app_widget_destination_2, diagramDetails.get(1).getDestination());
			views.setFloat(R.id.app_widget_destination_2, "setTextScaleX", getTextScaleX(diagramDetails.get(1).getDestination(),5));

			//特殊
			if(logic.isFinal(dbManager, diagramDetails.get(1))){
				views.setTextViewText(R.id.app_widget_extra_2, "最終");
			}else if(logic.isFirst(dbManager, diagramDetails.get(1))){
				views.setTextViewText(R.id.app_widget_extra_2, "始発");
			}else{
				views.setTextViewText(R.id.app_widget_extra_2, "  ");
			}
			
			// Tell the widget manager
			appWidgetManager.updateAppWidget(appWidgetId, views);

		} catch (Throwable e) {
			Log.e("station-dg", "error on update widget",e);
		} finally{
			dbManager.close();
		}

	}
	
	private static float getTextScaleX(String destination,int max){
		if(destination.length() <= max){
			return 1.0f;
		}
		BigDecimal moto = new BigDecimal(24 * max);
		BigDecimal b = moto.divide(new BigDecimal(destination.length() * 24),2, RoundingMode.CEILING);

		return b.floatValue();
	}
	
	private static int getColor(Context context ,String kindId){
		if("d1".equals(kindId)){
			return context.getResources().getColor(R.color.d1);
		}else if("d2".equals(kindId)){
			return context.getResources().getColor(R.color.d2);
		}else if("d3".equals(kindId)){
			return context.getResources().getColor(R.color.d3);
		}else if("d4".equals(kindId)){
			return context.getResources().getColor(R.color.d4);
		}else if("d5".equals(kindId)){
			return context.getResources().getColor(R.color.d5);
		}else if("d6".equals(kindId)){
			return context.getResources().getColor(R.color.d6);
		}else if("d7".equals(kindId)){
			return context.getResources().getColor(R.color.d7);
		}else if("d8".equals(kindId)){
			return context.getResources().getColor(R.color.d8);
		}else if("d9".equals(kindId)){
			return context.getResources().getColor(R.color.d9);
		}else if("d10".equals(kindId)){
			return context.getResources().getColor(R.color.d10);
		}else if("d11".equals(kindId)){
			return context.getResources().getColor(R.color.d11);
		}else if("d12".equals(kindId)){
			return context.getResources().getColor(R.color.d12);
		}else if("d13".equals(kindId)){
			return context.getResources().getColor(R.color.d13);
		}else if("d14".equals(kindId)){
			return context.getResources().getColor(R.color.d14);
		}else if("d15".equals(kindId)){
			return context.getResources().getColor(R.color.d15);
		}else if("d16".equals(kindId)){
			return context.getResources().getColor(R.color.d16);
		}else if("d17".equals(kindId)){
			return context.getResources().getColor(R.color.d17);
		}else if("d18".equals(kindId)){
			return context.getResources().getColor(R.color.d18);
		}else if("d19".equals(kindId)){
			return context.getResources().getColor(R.color.d19);
		}else if("d20".equals(kindId)){
			return context.getResources().getColor(R.color.d20);
		}else if("d21".equals(kindId)){
			return context.getResources().getColor(R.color.d21);
		}
		return context.getResources().getColor(R.color.d21);
	}
	
	public static class MyService extends Service{

		@Override
		public IBinder onBind(Intent intent) {
			return null;
		}

		@Override
		public void onStart(Intent intent, int startId) {
			Log.d("station-dg", "MyService.onStart#start");
			
			ComponentName thisWidget = new ComponentName(this, StationDGWidgetProvider.class);
			AppWidgetManager manager = AppWidgetManager.getInstance(this);
			int[] widgetIds = manager.getAppWidgetIds(thisWidget);
			for(int i : widgetIds){
				Log.d("station-dg", "widgetId:" + i);
				Diagram d = StationDGAppWidgetConfigure.loadTitlePref(this, i);
				if(d != null){
					updateAppWidget(this, manager, i, d);
				}
			}
			Log.d("station-dg", "MyService.onStart#finish");
		}
	}
}
