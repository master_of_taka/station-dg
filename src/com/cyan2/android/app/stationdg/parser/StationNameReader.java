package com.cyan2.android.app.stationdg.parser;

import java.io.StringReader;
import java.net.URLEncoder;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.ccil.cowan.tagsoup.HTMLSchema;
import org.ccil.cowan.tagsoup.Parser;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import com.cyan2.android.app.stationdg.domain.Station;


public class StationNameReader {

	private static final String SEARCH_URL = "http://www.navitime.co.jp/pcnavi/nd0121010.jsp?input=%s&ind=0&stationLimit=10&busLimit=10";
	
	private String searchName;

	public String getSearchName() {
		return searchName;
	}

	public void setSearchName(String searchName) {
		this.searchName = searchName;
	}
	
	public List<Station> get()throws Exception{
		DefaultHttpClient client = new DefaultHttpClient();
		
		String url = String.format(SEARCH_URL, URLEncoder.encode(searchName, "UTF-8"));
		HttpGet httpget = new HttpGet(url);
				
		HttpResponse response = client.execute(httpget);
		String html = EntityUtils.toString(response.getEntity());
		html = html.replaceAll("&nbsp;", " ");
		html = html.replaceAll("&nbsp", " ");
		
		XMLReader parser = new Parser();
		
		HTMLSchema schema = new HTMLSchema();
		parser.setProperty(Parser.schemaProperty, schema);
		
		StationSearchNameXmlReader serializer = new StationSearchNameXmlReader();
		
		parser.setContentHandler(serializer);
		// 属性へのデフォルト付与を抑制させます。
        parser.setFeature(Parser.defaultAttributesFeature, false);
        
		InputSource input = new InputSource();
		input.setCharacterStream(new StringReader(html));

		parser.parse(input);
		
		return serializer.getStations();

	}
}
