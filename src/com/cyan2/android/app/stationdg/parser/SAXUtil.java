package com.cyan2.android.app.stationdg.parser;

import org.xml.sax.Attributes;

public class SAXUtil {
	public static final String BLANK = "";
	public static final String SPACE = " ";
	public static final String DOUBLE_SPACE = "  ";
	public static final String EQUAL = "=";
	public static final String REGEX_RETURN = "\\n";
	public static final String REGEX_TAB = "\\t";
	
	public static final String TAG_NAME_A = "a";
	public static final String TAG_NAME_DIV = "div";
	public static final String TAG_NAME_UL = "ul";
	public static final String TAG_NAME_LI = "li";
	public static final String TAG_NAME_P = "p";
	public static final String TAG_NAME_H2 = "h2";
	public static final String TAG_NAME_SCRIPT = "script";
	
	public static final String ATTR_NAME_CLASS = "class";
	public static final String ATTR_NAME_ID = "id";

	
	
	public static boolean containsAttribute(Attributes attr,String key,String value){
		for(int i = 0;i < attr.getLength();i++){
			if(key.equals(attr.getQName(i)) && value.equals(attr.getValue(i))){
				return true;
			}
		}
		return false;
	}
	
	public static String getAttributeValue(Attributes attr,String key){
		for(int i = 0;i < attr.getLength();i++){
			if(key.equals(attr.getQName(i))){
				return attr.getValue(i);
			}
		}
		return null;
	}
	
	public static String toString(char[] chars, int start, int length){
		StringBuilder sb = new StringBuilder();
		for(int i = start ; i < start + length;i++){
			sb.append(chars[i]);
		}
		return sb.toString();
	}
}
