package com.cyan2.android.app.stationdg.domain;

import java.io.Serializable;

public class Line implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String lineCode;
	private String lineName;
	public String getLineCode() {
		return lineCode;
	}
	public void setLineCode(String lineCode) {
		this.lineCode = lineCode;
	}
	public String getLineName() {
		return lineName;
	}
	public void setLineName(String lineName) {
		this.lineName = lineName;
	}
	@Override
	public String toString() {
		return lineName;
	}
}
