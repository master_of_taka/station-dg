package com.cyan2.android.app.stationdg.domain;

import java.sql.Time;

public class DiagramDetail{
	private int diagramDetailId;
	private int diagramId;
	private String stationCode;
	private String lineCode;

	private String departureTime;
	private String kindId;
	private String kindName;
	private String destination;
	
	
	public int getDiagramDetailId() {
		return diagramDetailId;
	}
	public void setDiagramDetailId(int diagramDetailId) {
		this.diagramDetailId = diagramDetailId;
	}
	
	public int getDiagramId() {
		return diagramId;
	}
	public void setDiagramId(int diagramId) {
		this.diagramId = diagramId;
	}
	public String getStationCode() {
		return stationCode;
	}
	public void setStationCode(String stationCode) {
		this.stationCode = stationCode;
	}
	public String getLineCode() {
		return lineCode;
	}
	public void setLineCode(String lineCode) {
		this.lineCode = lineCode;
	}
	
	public String getDepartureTime() {
		return departureTime;
	}
	public void setDepartureTime(String departureTime) {
		this.departureTime = departureTime;
	}
	public String getKindId() {
		return kindId;
	}
	public void setKindId(String kindId) {
		this.kindId = kindId;
	}
	public String getKindName() {
		return kindName;
	}
	public void setKindName(String kindName) {
		this.kindName = kindName;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	
	@Override
	public String toString() {
		return departureTime.toString() + " " + kindName + " " + destination;
	}
}
