package com.cyan2.android.app.stationdg.domain;

import java.io.Serializable;

public class Station implements Serializable{
	private static final long serialVersionUID = 1L;

	private String stationCode;
	private String stationName;
	public String getStationCode() {
		return stationCode;
	}
	public void setStationCode(String stationCode) {
		this.stationCode = stationCode;
	}
	public String getStationName() {
		return stationName;
	}
	public void setStationName(String stationName) {
		this.stationName = stationName;
	}
	@Override
	public String toString() {
		return stationName;
	}
}
