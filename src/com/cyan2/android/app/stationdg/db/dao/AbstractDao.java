package com.cyan2.android.app.stationdg.db.dao;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

import com.cyan2.android.app.stationdg.db.Columns;
import com.cyan2.android.app.stationdg.db.DBManager;

public abstract class AbstractDao {
	
	private SQLiteDatabase db;
	
	public AbstractDao(SQLiteDatabase database) {
		this.db = database;
	}
	
	public SQLiteDatabase getDb() {
		return db;
	}

	protected long insert(ContentValues values){
		long key = db.insertOrThrow(getTableName(), null, values);
		return key;
	}
	
	protected void update(ContentValues values,int id){
		String where = String.format(Columns.ID + " = %d", id);
		
		db.update(getTableName(), values, where ,null);
	}
	
	protected void delete(int id){
		String where = String.format(Columns.ID + " = %d", id);
		
		db.delete(getTableName(), where ,null);
	}
	
	protected abstract String getTableName();
}	
