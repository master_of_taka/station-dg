package com.cyan2.android.app.stationdg.db;

public final class Tables {
	public static final String DIAGRAMS = "diagrams";
	public static final String DIAGRAM_DETAILS = "diagram_details";
	public static final String STATIONS = "stations";
	public static final String LINES = "lines";
}
